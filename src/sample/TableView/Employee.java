package sample.TableView;

/**
 * Created by Witold on 03.07.2017.
 */
public class Employee {
    private int employeeId;
    private String lastName;
    private String firstName;
    private String city;
    private String country;

    public Employee(int employeeId, String lastName, String firstName, String city, String country) {
        this.employeeId = employeeId;
        this.lastName = lastName;
        this.firstName = firstName;
        this.city = city;
        this.country = country;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }
}
