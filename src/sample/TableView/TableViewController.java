package sample.TableView;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.Callback;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Witold on 03.07.2017.
 */
public class TableViewController implements Initializable{
    @FXML
    TableView<Employee> tableViewEmployees;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initializeTableView();
    }

    private List<Employee> initializeEmployeesList()
    {
        List<Employee> employeeList = new ArrayList<>();

        employeeList.add(new Employee(1, "Kowalski", "Jan", "Wrocław","Polska"));
        employeeList.add(new Employee(2, "Kowalski", "Józef", "Wrocław","Polska"));
        employeeList.add(new Employee(14, "Nowak", "Kamil", "Poznań","Polska"));
        employeeList.add(new Employee(23, "Wójcik", "Pedro", "Madryt","Hiszpania"));

        return employeeList;
    }

    private void initializeTableView()
    {
        tableViewEmployees.setItems(FXCollections.observableArrayList(initializeEmployeesList()));

        TableColumn<Employee, Integer> idColumn = new TableColumn<>("Id");
        TableColumn<Employee, String> firstNameColumn = new TableColumn<>("Imię");
        TableColumn<Employee, String> lastNameColumn = new TableColumn<>("Nazwisko");
        TableColumn<Employee, String> cityColumn = new TableColumn<>("Miasto");
        TableColumn<Employee, String> countryColumn = new TableColumn<>("Kraj pochodzenia");


        idColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Employee,Integer>,ObservableValue<Integer>>() {

            @Override
            public ObservableValue<Integer> call(TableColumn.CellDataFeatures<Employee, Integer> param) {
                return new SimpleIntegerProperty(param.getValue().getEmployeeId()).asObject();
            }
        });

        firstNameColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Employee, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Employee, String> param) {
                return new SimpleStringProperty(param.getValue().getFirstName());
            }
        });

        lastNameColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Employee, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Employee, String> param) {
                return new SimpleStringProperty(param.getValue().getLastName());
            }
        });

        countryColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Employee, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Employee, String> param) {
                return new SimpleStringProperty(param.getValue().getCountry());
            }
        });
        cityColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Employee, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Employee, String> param) {
                return new SimpleStringProperty(param.getValue().getCity());
            }
        });

        tableViewEmployees.setEditable(true);
        tableViewEmployees.getColumns().setAll(idColumn, firstNameColumn, lastNameColumn, countryColumn, cityColumn);

    }
}
