package sample;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Side;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    @FXML
    AnchorPane sideMenu;
    @FXML
    SideMenuController sideMenuController;
    @FXML
    AnchorPane labelPane;
    @FXML
    LabelController labelPaneController;
    @FXML
    MenuButton closeButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //sideMenuController.setMainController(this);//Zabij mnie trzeba to zmienić!
    }
    @FXML
    public void setLabelMenuItemText(String text)
    {
        labelPaneController.setLabelText(text);
    }
    @FXML
    public void closeApp(ActionEvent event){
        Stage stage = (Stage) closeButton.getScene().getWindow();
    	stage.close();
    }

}
