package sample;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Witold on 03.07.2017.
 */
public class SideMenuController implements Initializable {
    @FXML
    private TreeView<MenuItem> treeViewMenu;

    MainController mainController;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initializeTreeViewMenu();
    }

    public void setMainController(MainController mainController)
    {
        this.mainController = mainController;
    }

    private void initializeTreeViewMenu() {
        TreeItem<MenuItem> dummyRoot = new TreeItem<MenuItem>(new MenuItem("root")); //Ten i tak sie nie pojawia, jest tylko do trzymania dwóch pod nim.

        TreeItem<MenuItem> rootItemSettings = new TreeItem<MenuItem>(new MenuItem("Ustawienia", new Image(getClass().getResourceAsStream("resources/ic_build_black_24dp.png"))));
        for (int i = 1; i < 4; i++) {
            TreeItem<MenuItem> item = new TreeItem<MenuItem>(new MenuItem("Item" + i));
            rootItemSettings.getChildren().add(item);
        }

        TreeItem<MenuItem> rootItemInfo = new TreeItem<MenuItem>(new MenuItem("Informacje", new Image(getClass().getResourceAsStream("resources/ic_info_black_24dp.png"))));
        for (int i = 1; i < 2; i++) {
            TreeItem<MenuItem> item = new TreeItem<MenuItem>(new MenuItem("Item" + i));
            rootItemInfo.getChildren().add(item);
        }

        TreeItem<MenuItem> rootItemDocuments = new TreeItem<MenuItem>(new MenuItem("Dokumenty", new Image(getClass().getResourceAsStream("resources/ic_description_black_24dp.png"))));
        for (int i = 1; i < 3; i++) {
            TreeItem<MenuItem> item = new TreeItem<MenuItem>(new MenuItem("Item" + i));
            rootItemDocuments.getChildren().add(item);
        }


        List<TreeItem> listTreeItems = new ArrayList<>(Arrays.asList(rootItemSettings, rootItemInfo, rootItemDocuments));

        for(int i = 0; i < listTreeItems.size(); i++) {
            TreeItem item = listTreeItems.get(i);
            item.setExpanded(true);
            //To zapobiega chowaniu sie menu, więc jak coś to usunąć
            item.addEventHandler(TreeItem.branchCollapsedEvent(), new EventHandler<TreeItem.TreeModificationEvent<Object>>() {
                @Override
                public void handle(TreeItem.TreeModificationEvent<Object> event) {
                    event.getTreeItem().setExpanded(true);
                }
            });
        }

        dummyRoot.getChildren().setAll(Arrays.asList(rootItemDocuments, rootItemInfo, rootItemSettings));
        treeViewMenu.setCellFactory(treeItem -> new TreeCell<MenuItem>() {
            @Override
            protected void updateItem(MenuItem item, boolean empty) {
                super.updateItem(item, empty);
                setDisclosureNode(null);

                if (empty) {
                    setText("");
                    setGraphic(null);
                } else {
                    setText(item.getText());
                    if(item.getImage() != null) {
                        setGraphic(new ImageView(item.getImage()));
                    }
                }
            }


        });


        treeViewMenu.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<TreeItem<MenuItem>>() {
            @Override
            public void changed(ObservableValue<? extends TreeItem<MenuItem>> observable, TreeItem<MenuItem> oldValue, TreeItem<MenuItem> newValue) {
                if(newValue.getValue().getImage() == null) //żeby nie wypisywac tych z ikonką.
                {
                    mainController.setLabelMenuItemText(newValue.getValue().getText());
                }
            }
        });
        treeViewMenu.setRoot(dummyRoot);
        treeViewMenu.setShowRoot(false); //Tutaj ustawiam żeby dummyRoot się nie wyświetlał
    }

}
