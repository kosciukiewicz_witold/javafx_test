package sample;

import javafx.scene.image.Image;

/**
 * Created by Witold on 02.07.2017.
 */
public class MenuItem {
    private String text;
    private Image image;


    public MenuItem(String text, Image image) {
        this.text = text;
        this.image = image;
    }

    public MenuItem(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public Image getImage() {
        return image;
    }


}
