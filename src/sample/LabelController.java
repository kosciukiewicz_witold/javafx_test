package sample;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Label;

import java.awt.*;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Witold on 03.07.2017.
 */
public class LabelController implements Initializable {
    @FXML
    Label labelMenuItem;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setLabelText(String text)
    {
        labelMenuItem.setText(text);
    }
}
